﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> AddAsync(T u)
        {
            u.Id = Guid.NewGuid();

            Data = Data.Append(u);

            return Task.FromResult(u.Id);
        }

        public Task UpdateAsync(T u)
        {
            var item = Data.Where(x => x.Id == u.Id).FirstOrDefault();

            item = u;

            Console.WriteLine($"Update entity with id = {u.Id.ToString()} Success...");
    
            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid id)
        {
            Data = Data.Where(x => x.Id != id).ToList();

            Console.WriteLine($"Delete entity id = {id} Success...");

            return Task.CompletedTask;
        }


    }
}