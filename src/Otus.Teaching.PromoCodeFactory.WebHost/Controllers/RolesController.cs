﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController : ControllerBase
    {
        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<RoleItemResponse>> GetRolesAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            var rolesModelList = roles.Select(x => 
                new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();

            return rolesModelList;
        }

        /// <summary>
        /// Создание новой роли
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Guid> CreateRoleAsync(RoleCreateUpdate r)
        {
            // Mapping
            Role newRole = new Role();
            newRole.Description = r.Description;
            newRole.Name = r.Name;

            return await _rolesRepository.AddAsync(newRole);
        }

        /// <summary>
        /// Обновить запись роли
        /// </summary>
        /// <param name="id"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateRoleAsync(Guid id, RoleCreateUpdate r)
        {
            var role = await _rolesRepository.GetByIdAsync(id);

            role.Name = r.Name;
            role.Description = r.Description;

            await _rolesRepository.UpdateAsync(role);

            return Ok();
        }

        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteRoleAsync(Guid id)
        {
            await _rolesRepository.DeleteAsync(id);

            return Ok();
        }
    }
}