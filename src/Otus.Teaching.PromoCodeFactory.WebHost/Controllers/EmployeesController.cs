﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <param name="empl"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Guid> CreateEmployeeAsync(EmployeeCreateUpdate empl)
        {
            // Mapping
            Employee newEmployee = new Employee();
            newEmployee.FirstName = empl.FirstName;
            newEmployee.LastName = empl.LastName;
            newEmployee.Email = empl.Email;
            newEmployee.AppliedPromocodesCount = empl.AppliedPromocodesCount;

            return await _employeeRepository.AddAsync(newEmployee);
        }

        /// <summary>
        /// Обновить запись сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employeeUpdate"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployeeAsync(Guid id, EmployeeCreateUpdate employeeUpdate)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee != null)
            {
                // Mapping
                employee.FirstName = employeeUpdate.FirstName;
                employee.LastName = employeeUpdate.LastName;
                employee.Email = employeeUpdate.Email;
                employee.AppliedPromocodesCount = employeeUpdate.AppliedPromocodesCount;

                await _employeeRepository.UpdateAsync(employee);

                return Ok();
            } else          
                return NotFound();
        }

        /// <summary>
        /// Удаление сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            if (await _employeeRepository.GetByIdAsync(id) != null)
            {
                await _employeeRepository.DeleteAsync(id);
                return Ok();
            }
                

            return NotFound();
        }
    }
}