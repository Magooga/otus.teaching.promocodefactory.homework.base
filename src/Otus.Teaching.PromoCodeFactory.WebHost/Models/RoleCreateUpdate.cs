﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class RoleCreateUpdate
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
