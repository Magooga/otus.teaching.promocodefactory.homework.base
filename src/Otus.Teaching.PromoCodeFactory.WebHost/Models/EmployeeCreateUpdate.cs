﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeCreateUpdate
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}
